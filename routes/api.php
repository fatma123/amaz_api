<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api', 'middleware' => 'localization'], function () {

//AUTH
    Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {
        Route::post('register', 'AuthController@register');
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
    });

//GENERAL
    Route::POST('ProductSearch', 'SearchController@ProductSearch');
    Route::POST('filter', 'SearchController@Filter');

    Route::resource('settings', 'SettingController');


    Route::resource('categories', 'CategoriesController');
    Route::resource('sub_categories', 'SubCategoriesController');

//PRODUCTS
    Route::resource('products', 'ProductsController');
    Route::post('rates/{id}', 'RateController@store');

//CART
    Route::resource('cart','CartController');
    Route::post('client-details', 'CartController@PostDetails');

    Route::resource('questions', 'QuestionsController');
    Route::Post('post-questionnaires/{id}', 'QuestionsController@store');
    Route::Post('answer_questionnaires', 'QuestionsController@answer_questionnaires');


});