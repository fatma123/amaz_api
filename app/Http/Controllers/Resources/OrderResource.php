<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
//        dd((float)$this->service->rate->avg('value'));
        $admin=User::where('role','admin')->first();
        return[
            'id'=>$this->id,
            'name' => $this->name(),
            'description'=>$this->description,
            'status'=>$this->status,
            'user_id'=>$this->user_id,
            'user_name'=>$this->user->name,
            'user_image'=>getimg($this->user->image),
            'service_id'=>$this->service_id,
            'category_id'=>$this->service->category->id,
            'service_name'=>$this->service->name,
            'service_image'=>getimg($this->service->image),
            'created_at'=>$this->created_at->format('Y-m-d H:i'),
            'files'=>$this->files->transform(function ($q){return['file'=> getimg($q->file),'type'=>$q->type];}),
            'refers'=> UserResource::collection($this->refers),
            'rate'=>blank($this->service->rate)?0:$this->service->rate->avg('value'),
            'user_rate'=>blank($this->rate)?0:$this->rate->value,
            'admin_id'=>$admin->id,
            'admin_name'=>$admin->name,
            'admin_image' => getimg($admin->image),
            'is_rated' => $this->rate()->exists()
        ];
    }
}
