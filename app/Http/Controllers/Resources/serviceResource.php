<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class serviceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'price'=>$this->price,
            'description'=>$this->description,
            'image'=>getimg($this->image),
            'is_active'=>$this->is_active,
            'rate'=>$this->rate->avg('value')==null?0:$this->rate->avg('value')
        ];
    }
}
