<?php


namespace App\Http\Traits;

use App\Category;
use Illuminate\Http\Request;

trait CategoryOperations
{
    public function StoreCategory($request)
    {

        $inputs = $request->all();
        if ($request->image != null)
        {
            if ($request->hasFile('image')) {
                $picture = uploader($request,'image');
                $inputs['image'] = $picture;
            }
        }
        $category= Category::create($inputs);
        return $category;
    }

}