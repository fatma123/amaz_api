<?php


namespace App\Http\Traits;
use App\User;
use Hash;

trait UserOperation
{
   public function RegisterUser($request)
  {
      $inputs = $request->all();
      if ($request->image != null)
      {
          if ($request->hasFile('image')) {
              $picture = uploader($request,'image');
              $inputs['image'] = $picture;
          }
      }

      $inputs['password']=Hash::make($request->password);

      $user=User::create($inputs);
      $inputs['is_admin']=0;
      return $user;
  }

}