<?php


namespace App\Http\Traits;


use App\Address;
use App\Qualification;
use App\User;
use Hash;
use Illuminate\Http\Request;

trait QualificationOperation
{
   public function RegisterQualification($request)
  {
      $inputs = $request->all();
      if ($request->image != null)
      {
          if ($request->hasFile('image')) {
              $picture = uploader($request,'image');
              $inputs['image'] = $picture;
          }
      }

      return Qualification::create($inputs);
  }

    public function UpdateQualification($qualification, $request)
    {
        $inputs = $request->except('image');
        if ($request->image != null)
        {
            if ($request->hasFile('image')) {
                $picture = uploader($request,'image');
                $qualification->update(['image' => $picture]);
            }
        }
        return $qualification->update($inputs);
    }


}