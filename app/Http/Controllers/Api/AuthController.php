<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;
use App\Http\Traits\ApiResponses;
use App\Http\Traits\UserOperation;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use JWTAuth;

class AuthController extends Controller
{
use ApiResponses,UserOperation;

    public function __construct()
    {
        $this->middleware('auth:api')->except(['login','register']);
    }

    public function register(Request $request){

        $rules = [
            'name'=>'required|string|max:191|unique:users',
            'email'=>'required|email:max:191|unique:users',
            'phone'=>'required|numeric|unique:users',
            'password'=>'required|string|max:191',
            'image'=>'sometimes|image',
        ];

        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}
        {$user =$this->RegisterUser($request);}
        $user=User::find($user->id);
        $token = JWTAuth::fromUser($user);
        $user['token'] = $token;
        $user =  new UserResource($user);
        if ($token && $user) {return $this->createdResponse($user);}
        $this->unKnowError();

    }


    public function login(Request $request){

        $rules = [
            'email'=>'required|email:max:191|exists:users,email',
            'password'=>'required|string|max:191',
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}
        $credentials = request(['email', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {
            return $this->apiResponse(null, 'عفوا هناك خطا في كلمة المرور او البريد الالكتروني', 400);
        }
        if (api()->user()->is_admin == 0) {
            return $this->apiResponse(null, 'عفوا  , يجيب تفعيل حسابكم من قِبل الادارة', 402);
        }

        $user = api()->user();
        $user->save();
        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */

    protected function respondWithToken($token)
    {
        $user=api()->user();

        $user['token']=$token;

        return $this->apiResponse(new UserResource($user));
    }

    public function logout()
    {
        auth('api')->logout();

        return $this->apiResponse(['message' => 'Successfully logged out']);
    }



}
