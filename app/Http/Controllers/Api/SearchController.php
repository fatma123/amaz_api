<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ProductsResource;
use App\Http\Resources\serviceResource;
use App\Http\Traits\ApiResponses;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class SearchController extends Controller
{
    use ApiResponses ;

    public function ProductSearch(Request $request)
    {

        if (isset($request['query'])) {
            $this->validate($request, [
                'query' => 'required|string'
            ]);
            $result = Product::query()
                ->where('ar_name', 'LIKE', "%{$request['query']}%")->latest()->get();
        }

        if (isset($request['query_en'])) {
            $this->validate($request, [
                'query_en' => 'required|string'
            ]);
            $result =Product::query()
                ->where('en_name', 'LIKE', "%{$request['query_en']}%")->latest()->get();

        }

        return $this->apiResponse($result);


    }

    public function Filter(Request $request){

        $products = Product::query();

        if ($request->has('sub_category_id')) {
            $products->where('sub_category_id', $request->sub_category_id);
        }

        if ($request->hasAny('min_value', 'max_value')) {
            $products->whereBetween('price', [$request->min_value, $request->max_value]);
        }

        return $this->apiResponse(ProductsResource::collection($products->get()));

    }

}
