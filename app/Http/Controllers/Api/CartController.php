<?php

namespace App\Http\Controllers\Api;

use App\Cart;
use App\CartItem;
use App\Http\Resources\cartItemsResource;
use App\Http\Resources\cartResource;
use App\Http\Traits\ApiResponses;
use App\Http\Traits\CartOperations;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class CartController extends Controller
{

    use ApiResponses, CartOperations;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items = CartItem::where('cart_id', $id)->first();
        if (!$items->exists()) return $this->notFoundResponse();
        return $this->apiResponse(new cartItemsResource($items));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function PostDetails(Request $request)
    {

        $rules = [
            'client_name' => 'required|string|max:191',
            'client_phone' => 'required|',
            'notes' => 'required|string|',
            'status' => 'required|in:on_processing,prepared,refused,received,not_received'
        ];
        $validation = $this->apiValidation($request, $rules);
        if ($validation instanceof Response) {
            return $validation;
        }
        $cart = $this->StoreDetails($request);

        $cart = new cartResource($cart);

        if ($cart) {
            return $this->createdResponse($cart);
        }
        $this->unKnowError();

    }

}
