<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\QuestionsResource;
use App\Http\Traits\ApiResponses;
use App\Question;
use App\Questionnaires;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class QuestionsController extends Controller
{
    use ApiResponses;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->apiResponse(QuestionsResource::collection($questions=Question::all()));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $rules=[
//            'question_id'=>'required|exists:questions,id',
            'answer'=>'required|in:excellent,very_good,good,bad,angry_bad',
            'cart_id'=>'required|exists:carts,id'
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        $inputs = $request->all();
        $question=Question::find($id);
        $inputs['question_id']=$id;
        Questionnaires::create($inputs);
        return $this->apiResponse('تمت اجابة السؤال بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function answer_questionnaires(Request $request){
        $rules=[
            'answer'=>'array',
            'answer.*'=>'required|in:excellent,very_good,good,bad,angry_bad',
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}

        $inputs = $request->all();
//        dd($inputs);
        foreach ($request->answer as $key => $value) {
            Questionnaires::create([
                'question_id' => $key,
                'cart_id'=>$inputs["cart_id"],
                'answer' => $value,

            ]);
        }
//        dd($inputs);

        return $this->apiResponse('تمت اجابة الاستبيانات بنجاح');

    }
}
