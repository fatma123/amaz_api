<?php

namespace App\Http\Controllers\Admin;

use App\Discount;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiscountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discounts=Discount::all();
        return view('admin.discounts.index',compact('discounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products=Product::pluck('ar_name','id')->toArray();
        return view('admin.discounts.add',compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'product_id'=>'required|exists:products,id',
            'value'=>'required|numeric',
            'expired_date'=>'required|date'
        ]);
        $inputs=$request->all();
        Discount::create($inputs);
        alert()->success('تم اضافة الخصم بنجاح !')->autoclose(5000);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products=Product::pluck('ar_name','id')->toArray();

        $discount=Discount::find($id);

        return view('admin.discounts.edit',compact('discount','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $discount=Discount::find($id);
        $this->validate($request,[
            'product_id'=>'required|exists:products,id',
            'value'=>'required|numeric',
            'expired_date'=>'required|date'
        ]);
        $inputs=$request->all();
        $discount->update($inputs);
        popup('update');
        return redirect('dashboard/discounts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $discount=Discount::find($id);
        if ($discount){

            $discount->delete();
            alert()->success('تم حذف الخصم بنجاح');
            return back();
        }
        alert()->error('الخصم الذى  تحاول حذفه غير موجود');
        return back();
    }
}
