<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = popup(['rules'=>[
            'name'=>'required|string|max:191',
            'email'=>'required|email|max:191|unique:users',
            'phone'=>'required|string|max:191',
            'password'=>'required|string|min:6|max:191|confirmed',
            'address'=>'required',
            'lat'=>'required',
            'lng'=>'required',
            'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]]);
        if ($validator){
            return back()->withInput($request->all());
        }

        $inputs = $request->except('image','lat','lng','address');

        if ($request->has('image')){
            $inputs['image'] = uploader($request,'image');
        }

        if ($request->email_verified_at == 1)
            $inputs['email_verified_at'] = now();
        else
            $inputs['email_verified_at'] = null;

        if ($request->has('password'))
            $inputs['password'] = bcrypt($request->password);

        $user = User::create($inputs);

        $user->address()->create(['lat'=>$request->lat,'lng'=>$request->lng,'address'=>$request->address]);

        popup('add');

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $validator = popup(['rules'=>[
            'name'=>'required|string|max:191',
            'email'=>'required|email|max:191|unique:users,email,'.$user->id,
            'phone'=>'required|string|max:191',
            'password'=>'nullable|string|min:6|max:191|confirmed',
            'address'=>'required',
            'lat'=>'required',
            'lng'=>'required',
            'image'=>'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]]);
        if ($validator){
            return back()->withInput($request->all());
        }

        $inputs = $request->except('image','lat','lng','address','password');

        if ($request->has('image')){
            $inputs['image'] = uploader($request,'image');
        }else{
            $inputs['image'] = $user->image;
        }

        if ($request->email_verified_at == 1)
            $inputs['email_verified_at'] = now();
        else
            $inputs['email_verified_at'] = null;

        if ($request->has('password'))
            $inputs['password'] = bcrypt($request->password);

        $user->update($inputs);

        $user->address()->update(['lat'=>$request->lat,'lng'=>$request->lng,'address'=>$request->address]);

        popup('update');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        popup('delete');
        return back();
    }
}
