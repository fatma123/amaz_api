<?php

namespace App\Http\Controllers\Admin;

use App\Cart;
use App\CartItem;
use App\Http\Controllers\Controller;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function clients()
    {

        $client = Cart::pluck('client_name', 'id')->toArray();
        $clients = Cart::paginate(10);
        return view('admin.reports.clients', compact('client', 'clients'));
    }

//    public function clientsSearch(Request $request)
//    {
//        $client = Cart::pluck('client_name', 'id')->toArray();
//        $this->validate($request, [
//            'id'=>'required',
//            'from'=>'required',
//            'to'=>'required'
//        ]);
//
//        $inputs = $request->all();
//        $client_name = $request->id;
////        dd($client_name);
//        $from = $inputs['from'];
//        $to = $inputs['to'];
//
//        $clients = Cart::where('id',$client_name)->whereBetween('created_at', [$from, $to])->paginate(10);
//
//        return view('admin.reports.clients', compact('client', 'clients'));
//
//    }


    public function products()
    {

        $product = Product::pluck('ar_name', 'id')->toArray();
        $products = Product::all();
        $cart_items = CartItem::all();
        return view('admin.reports.products', compact('products', 'cart_items', 'product'));
    }

    public function productSearch(Request $request)
    {
        $product = Product::pluck('ar_name', 'id')->toArray();
        $inputs = $request->all();
        $product_id = $inputs['product_id'];
        $from = $inputs['from'];
        $to = $inputs['to'];
        $items = CartItem::whereBetween('created_at', [$from, $to])->pluck('product_id');
        $products = CartItem::where('product_id', $product_id)->get();
//        dd($products);
        return view('admin.reports.products', compact('carts', 'products', 'product'));

    }

    public function ordersSum($month, $year)
    {
        $sum = CartItem::whereMonth('created_at', date($month))->whereYear('created_at', date($year))->sum('price');
        return $sum;
    }

    public function dailyReports(Request $request)
    {

        $all_orders = \App\CartItem::sum('price');
        $today_orders = \App\CartItem::whereDate('created_at', now()->toDateString())->sum('price');
        $this_month_orders = $this->ordersSum(date("m"), date("Y"));
        $this_year_orders= \App\CartItem::whereYear('created_at', now()->format('Y'))->sum('price');
//dd($this_year_orders);


        $last_month_orders = $this->ordersSum(date("m", strtotime("-1 month")), date("Y", strtotime("-1 month")));

        $last_3month_orders = $last_month_orders + $this->ordersSum(date("m", strtotime("-2 month")), date("Y", strtotime("-2 month"))) + $this->ordersSum(date("m", strtotime("-3 month")), date("Y", strtotime("-3 month")));

//        dd($this_month_orders);
        $reports = [
            //الإيرادات
            'orders' => ['all' => $all_orders, 'this_month' => $this_month_orders, 'last_month' => $last_month_orders, 'last_3month' => $last_3month_orders, 'today' => $today_orders,'year'=>$this_year_orders],

        ];

        return view('admin.reports.daily_reports', compact('reports'));
    }
}
