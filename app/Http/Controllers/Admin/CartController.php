<?php

namespace App\Http\Controllers\Admin;
use App\Cart;
use App\CartItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carts=Cart::paginate(10);
        return view('admin.cart.index',compact('carts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cart_item=CartItem::where('cart_id',$id)->get();
        return view('admin.cart.view',compact('cart_item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cart=Cart::find($id);
        return view('admin.cart.edit',compact('cart'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cart=Cart::find($id);
        $this->validate($request,[
            'status'=>'required'
        ]);
        $inputs=$request->all();
        $cart->update($inputs);
        popup('update');
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart=Cart::find($id);
        if ($cart){
            $cart->delete();
            alert()->success('تم حذف الطلب  بنجاح');
            return back();
        }
        alert()->error('الطلب الذي تحاول حذفه غير موجوده ');
        return back();
    }

    public function ajax_prepared($id)
    {
        $cart = Cart::findOrFail($id);
        $cart->status = 'prepared';
        $cart->save();
        return response()->json($cart);
    }

    public function ajax_onProcessing($id)
    {
        $cart = Cart::findOrFail($id);
        $cart->status = 'on_processing';
        $cart->save();
        return response()->json($cart);
    }

    public function ajax_refused($id)
    {
        $cart = Cart::findOrFail($id);
        $cart->status = 'refused';
        $cart->save();
        return response()->json($cart);

    }

    public function ajax_received($id)
    {
        $cart = Cart::findOrFail($id);
        $cart->status = 'received';
        $cart->save();
        return response()->json($cart);

    }
    public function ajax_not_received($id)
    {
        $cart = Cart::findOrFail($id);
        $cart->status = 'not_received';
        $cart->save();
        return response()->json($cart);

    }

}
