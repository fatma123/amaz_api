<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $SubCategories = SubCategory::all();
        return view('admin.subcategories.index',compact('SubCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::pluck('ar_name','id')->toArray();
        return view('admin.subcategories.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = popup(['rules'=>[
            'ar_name'=>'required',
            'en_name'=>'required',
            'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'category_id'=>'required|exists:categories,id'
        ]]);

        if ($validator)
            return back()->withInput($request->all());

        $inputs = $request->except('image');

        if ($request->has('image'))
            $inputs['image'] = uploader($request,'image');

        SubCategory::create($inputs);
        popup('add');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories=Category::pluck('ar_name','id')->toArray();
        $sub_category = SubCategory::find($id);

        return view('admin.subcategories.edit',compact('sub_category','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sub = SubCategory::find($id);
        $validator = popup(['rules'=>[
            'ar_name'=>'required',
            'en_name'=>'required',
            'image'=>'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'category_id'=>'required'
        ]]);

        if ($validator)
            return back()->withInput($request->all());

        $inputs = $request->except('image');

        if ($request->has('image'))
            $inputs['image'] = uploader($request,'image');

        $sub->update($inputs);
        popup('update');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SubCategory::find($id)->delete();
        popup('delete');
        return back();
    }
}
