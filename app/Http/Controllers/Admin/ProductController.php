<?php

namespace App\Http\Controllers\Admin;
use App\Product;
use App\Category;
use App\SubCategory;
use App\ProductGallery;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::paginate(10);
        return view('admin.products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::pluck('ar_name','id')->toArray();
        $sub_category=SubCategory::pluck('ar_name','id')->toArray();
        return view('admin.products.add',compact('sub_category','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        $this->validate($request,[

            'ar_name'=>'required|string|max:255',
            'en_name'=>'required|string|max:255',
            'ar_description'=>'required|string|max:255',
            'en_description'=>'required|string|string|max:255',
            'price'=>'required|numeric',
            'is_active'=>'required',
            'expired_date'=>'sometimes',
            'discount'=>'sometimes',
            'quantity'=>'required',
            'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'sub_category_id'=>'required|exists:sub_categories,id',
            'files.*'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $inputs = $request->except('image');

        if ($request->has('image'))
            $inputs['image'] = uploader($request,'image');

        $product=Product::create($inputs);
        if($request->hasFile('files')) {
            foreach ($request['files'] as $key => $item) {
                $imageName = $path =\Storage::disk('public')->putFile('photos',$item);
                $ads_image = new ProductGallery();
                $ads_image->product_id = $product->id;
                $ads_image->image =  $imageName;
                $ads_image->save();
            }
        }

        alert()->success('تم اضافة منتج بنجاح !')->autoclose(5000);
        return redirect('dashboard/products');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product=Product::find($id);
        return view('admin.products.view',compact('product'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories=Category::pluck('ar_name','id')->toArray();
        $product=Product::find($id);
        $sub_category=SubCategory::pluck('ar_name','id')->toArray();
        return view('admin.products.edit',compact('product','categories','sub_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product=Product::find($id);
        $this->validate($request,[
            'ar_name'=>'required|string|max:255',
            'en_name'=>'required|string|max:255',
            'ar_description'=>'required|string|max:255',
            'en_description'=>'required|string|string|max:255',
            'price'=>'required|numeric',
            'is_active'=>'required',
            'expired_date'=>'sometimes',
            'discount'=>'sometimes',
            'quantity'=>'nullable',
            'sub_category_id'=>'required|exists:sub_categories,id',
            'files.*'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $inputs = $request->except('image');

        if ($request->has('image'))
            $inputs['image'] = uploader($request,'image');

        $product->update($inputs);

        if($request->hasFile('files')) {
            foreach ($request['files'] as $key => $item) {
                $imageName = $path =\Storage::disk('public')->putFile('photos',$item);
                $ads_image = new ProductGallery();
                $ads_image->product_id = $product->id;
                $ads_image->image =  $imageName;
                $ads_image->save();
            }
        };

        popup('update');
        return redirect('dashboard/products');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::find($id);
        if ($product){

            $product->delete();
            alert()->success('تم حذف المنتج بنجاح');
            return back();
        }
        alert()->error('المنتج الذى  تحاول حذفه غير موجود');
        return back();
    }

    public function CategoryAjax($id){
        $sub_category=SubCategory::select('ar_name','id')->where('category_id',$id)->get();
        return response()->json($sub_category);
    }
}
