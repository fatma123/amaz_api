<?php

namespace App\Http\Controllers\Admin;

use App\Question;
use App\Questionnaires;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionnairesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions=Question::distinct()->get();
        return view('admin.questionnaires.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.questionnaires.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Questionnaires::find($id)->delete();
        popup('delete');
        return back();
    }


    public function search(Request $request)
    {
//        if (isset($request['query'])) {
////            dd($request);
//            $this->validate($request, [
//                'query' => 'required'
//            ]);
////            dd(\App\Questionnaires::query());
//            $questionnaires = \App\Questionnaires::query()
//                ->where('answer', 'LIKE', "%{$request['query']}%")->latest()->get();
//
//        }else{
//            $questionnaires=\DB::table('questionnaires')
//                ->select('question_id',DB::raw('count(*) as total'))
//                ->groupBy('question_id')
//                ->get();
//        }
//
//
//        return view('admin.questionnaires.index', compact('questionnaires','answer'));
//    }

    }
}
