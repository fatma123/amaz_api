<?php

namespace App\Http\Controllers\Admin;
use App\User;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins=User::all();
        return view('admin.admins.index',compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admins.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string',
            'email'=>'required||string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone'=>'required|string|max:255|unique:users',
            'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        $image=uploader($request,'image');
        $inputs=$request->all();
        $inputs['image']=$image;
        $inputs['password']=Hash::make($request->password);
        User::create($inputs);
        alert()->success('تم اضافة الادمن بنجاح !')->autoclose(5000);
        return redirect('dashboard/admins');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user= User::find($id);
        return view('admin.admins.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin= User::find($id);
        $this->validate($request,[
            'name'=>'required|string',
            'password' => 'nullable|min:6|confirmed',
            'email'=>'required||string|email|max:255|unique:users,email,'.$admin->id,
            'phone'=>'required|numeric|unique:users,phone,'.$admin->id,
            'image'=>'nullable'
        ]);
        $inputs = $request->all();
        if($request->hasFile('image'))
        {
            $image = uploader($request,'image');
            $inputs['image']=$image;
        }
//        dd($image);
        $inputs['password']=Hash::make($request->password);

        $admin->update($inputs);
        alert()->success('تم تعديل بيانات الادمن بنجاح !')->autoclose(5000);

        return redirect('dashboard/admins');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin=User::find($id);
        dd($admin);
        if($admin->email=='admin@admin.com'){
            alert()->error('لايمكن حذف الادمن  ');
            return back();
        }
        if ($admin){
            $admin->delete();
            alert()->success('تم حذف الادمن بنجاح');
            return back();
        }
        alert()->error('الادمن الذى تحاول حذفه غير موجود');
        return back();
    }

    public function activate($id)
    {
        $employee =User::findOrFail($id);
        $employee->is_admin = '1';
        $employee->save();
        alert()->success('تم جعله عضو إدارة  !')->autoclose(5000);
        return back();
    }


    public function deActivate($id)
    {
        $employee =User::findOrFail($id);
        $employee->is_admin = '0';
        $employee->save();
        alert()->success('تم تحويله لمستخدم   !')->autoclose(5000);
        return back();
    }
}
