<?php

namespace App\Http\Controllers\Website;

use App\Cart;
use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductGallery;
use App\Question;
use App\Questionnaires;
use App\Rate;
use App\SubCategory;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {

        $categories = Category::all();
        return view('website.index', compact('categories'));
    }

    public function subcategories($id)
    {
        $category=Category::find($id);
        $sub_categories=SubCategory::where('category_id',$id)->get();
        return view('website.subcategories', compact('sub_categories'));
    }

    public function products($id)
    {
        $subcategory=SubCategory::find($id);
        $_products = Product::where('sub_category_id',$id)->get();
//        dd($_products);
        return view('website.products', compact('_products'));
    }

    public function single_product($id)
    {
        $product = Product::find($id);
        $rate = Rate::where('product_id', $id)->get();
        $files = ProductGallery::where('product_id', $id)->get();
        return view('website.single_product', compact('product', 'files', 'rate'));
    }

    public function postRate(Request $request, $id)
    {
        $inputs = $request->all();
        $inputs['product_id'] = $id;
        Rate::create($inputs);
        alert()->success(trans('translation.Your rate has been added successfully'))->autoclose(5000);
        return back();
    }

    public function Questionnaires($id)
    {
        $questions = Question::all();
        $cart= Cart::find($id);
        return view('website.questions', compact('questions','cart'));
    }

    public function PostQuestionnaires(Request $request)
    {

        $items = session()->has('cart') ? session()->get('cart') : [];
        $this->validate($request, [
            "answer" => 'required'
        ]);
        $inputs = $request->all();
//        dd($inputs["cart_id"]);
        foreach ($request->answer as $key => $value) {
            Questionnaires::create([
                'question_id' => $key,
                'cart_id'=>$inputs["cart_id"],
                'answer' => $value,

            ]);
        }
        return redirect('/questionnaires-reply/' .$request->cart_id);
    }

    public function QuestionnairesReply($id)
    {
//        $order = Cart::where('id',$id)->get();
//dd($order);
        return view('website.thanks', compact('id'));
    }

    public function addToCart(Request $request, $id)
    {
        $product = Product::find($id);
        $this->validate($request, [
            'price' => 'required|numeric',
            'quantity' => 'required|string',
        ]);
        $cart = Cart::create(['status' => 'on_processing', 'discount' => 0]);

    }

    public function about()
    {

        return view('website.about');
    }



    public function filter(Request $request, $_products = null)
    {
        // dd($request->all());

        $category = Category::all();
        $sub_category = SubCategory::whereCategoryId($category)->get();
        $product = Product::whereIn('sub_category_id', $sub_category)->orderBy('created_at', 'desc')->get();

        $products = Product::query();

        if ($request->subCategory) {
            $products->where('subcategory_id', $request->input('subcategory_id'));
        }
        if ($request->hasAny('min_value', 'max_value')) {
            $products->whereBetween('price', [$request->min_value, $request->max_value]);
        }

        $products = $products->get();

        return view('website.filter', compact('products'));


    }

    public function search(Request $request)
    {

        if (isset($request['query'])) {
            $this->validate($request, [
                'query' => 'required|string'
            ]);
            $result = \App\Product::query()
                ->where('ar_name', 'LIKE', "%{$request['query']}%")->latest()->get();
        }

        if (isset($request['query_en'])) {
            $this->validate($request, [
                'query_en' => 'required|string'
            ]);
            $result = \App\Product::query()
                ->where('en_name', 'LIKE', "%{$request['query_en']}%")->latest()->get();

        }

        return view('website.search', compact('result'));
    }

}
