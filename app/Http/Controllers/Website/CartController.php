<?php

namespace App\Http\Controllers\website;
use App\Notifications\OrderNotification;
use App\User;
use App\Cart;
use App\Http\Controllers\Controller;
use App\Payment;
use App\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function store(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = \Session::has('cart') ? \Session::get('cart') : null;
        $cart = new Payment($oldCart);
//        $items = $cart->items;

        if ($request->quantity <= $product->quantity) {
            $cart->add($product, $product->id, $request->quantity);

            foreach ($cart->items as $item) {
                $product = Product::find($item['item']->id);
                $product->decrement('quantity', $item['qty']);
            }
            $request->session()->put('cart', $cart);
            popup('add');


        } else {
            alert()->success(trans('translation.Ordered Quantity not found'))->autoclose(5000);
        }

        return back();
    }

    public function order()
    {
        $items = session()->has('cart') ? session()->get('cart') : [];
//        dd($items);
        return view('website.cart', compact('items'));
    }

    public function clientDetails()
    {

        return view('website.client_details');
    }


    public function PostClientDetails(Request $request)
    {
        $items = session()->has('cart') ? session()->get('cart') : [];
//        dd($request->all());
        $this->validate($request, [
            'client_name' => 'required|string|max:255',
            'client_phone' => 'required|numeric',
            'notes' => 'nullable|string'
        ]);

        $inputs = $request->all();
        $cart = Cart::create($inputs);
        foreach ($items->items as $item) {
            $cart->items()->create([
                'price' => $item['price'],
                'quantity' => $item['qty'],
                'product_id' => $item['item']->id

            ]);
        }
        $user=User::where('is_admin',1)->inRandomOrder()->first();
        if($user)
            \Notification::send($user, new OrderNotification($cart));
        \Illuminate\Support\Facades\Session::forget('cart');
        return redirect('/questionnaires/'.$cart->id);
    }


    public function CartIncrease(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = \Session::has('cart') ? \Session::get('cart') : null;
        $cart = new Payment($oldCart);

        $cart->increaseByOne($product, $product->id, $request->quantity);

        $request->session()->put('cart', $cart);
//        dd($request);
        return response()->json(['price' => Product::find($id)->priceAfterDiscount()]);
    }

    public function CartDecrease(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = \Session::has('cart') ? \Session::get('cart') : null;
        $cart = new Payment($oldCart);
        $price = $cart->reduceByOne($request->id);
        if (count($cart->items) > 0) {
            \Session::put('cart', $cart);
        } else {
            \Session::forget('cart');
        }
        return response()->json(['price' => $price]);
    }
}
