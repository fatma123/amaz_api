<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $fillable = [
        'product_id','value','expired_date'
    ];
    public function products(){
        return $this->belongsTo(Product::class,'product_id');
    }
}
