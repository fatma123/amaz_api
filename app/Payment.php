<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment
{
    public $items = null;
    public $totalPrice = 0;
    public $has_coupon = false;
    public $coupon_discount = 0;

    public function __construct($oldCart)
    {
        if($oldCart)
        {
            $this->items = $oldCart->items;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }
    public function add($item ,$id,$qty=0)
    {

        $price = $item->price;

        $storedItem = [ 'price'=>$price , 'item'=>$item,'qty'=>$qty];
        $storedItem['qty']= $qty;
        $storedItem['price'] = $price;
        $this->items[$id] = $storedItem;
        $this->totalPrice += ($price*$qty);

    }

    public function increaseByOne($item ,$id,$qty)
    {
        $price = $item->price;
        $storedItem = [ 'price'=>$price,'item'=>$item,'qty'=>$qty];
        $storedItem['qty']= $qty+1  ;
        $storedItem['price'] = $price;
        $this->items[$id] = $storedItem;
        $this->totalPrice += ($price*$qty);

    }

    public function reduceByOne($id)
    {
        $price = $this->items[$id]['item']['price'] - ($this->items[$id]['item']['price']);
        $this->items[$id]['qty']--;
        $this->items[$id]['price'] -= $price;
        $this->totalPrice -= $price;
        return $price;
    }

}
