<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = [
        'rate','product_id'
    ];

    public  function products(){

        return $this->belongsTo(Product::class,'product_id');
    }
}
