<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable =['ar_question','en_question'];

    public function question()
    {
        if (app()->getLocale() == 'en')
            return $this->en_question;
        else
            return $this->ar_question;
    }

    public function answers(){

        return $this->hasMany(Questionnaires::class);
    }

    public function all_questions()
    {
        return $this->answers()->count();
    }

    public function excellent_questions()
    {
        $all_question=$this->all_questions();
        if ($this->all_questions()==0){
            return 0;
        }
        return $this->answers()->where('answer','excellent')->count()/$all_question;
    }

    public function veryGood_questions()
    {
        $all_question=$this->all_questions();
        if ($this->all_questions()==0){
            return 0;
        }
        return $this->answers()->where('answer','very_good')->count()/$all_question;
    }

    public function good_questions()
    {
        $all_question=$this->all_questions();
        if ($this->all_questions()==0){
            return 0;
        }
        return $this->answers()->where('answer','good')->count()/$all_question;
    }

    public function bad_questions()
    {
        $all_question=$this->all_questions();
        if ($this->all_questions()==0){
            return 0;
        }
        return $this->answers()->where('answer','bad')->count()/$all_question;
    }

    public function angry_questions()
    {
        $all_question=$this->all_questions();
        if ($this->all_questions()==0){
            return 0;
        }
        return $this->answers()->where('answer','angry_bad')->count()/$all_question;
    }


}
