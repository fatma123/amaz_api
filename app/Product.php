<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'ar_name', 'en_name','ar_description','en_description','price','is_active','sub_category_id','discount','expired_date','image','quantity'
    ];
    public function sub_category(){

        return $this->belongsTo(SubCategory::class);
    }
    public function name()
    {
        if (app()->getLocale() == 'en')
            return $this->en_name;
        else
            return $this->ar_name;
    }

    public function description()
    {
        if (app()->getLocale() == 'en')
            return $this->en_description;
        else
            return $this->ar_description;
    }
    public function gallery()
    {
        return $this->hasMany(ProductGallery::class);
    }

    public function discounts(){
        return $this->hasMany(Discount::class,'product_id','id');
    }

    public function priceAfterDiscount(){

        $total = $this->price-$this->discount;
        return $total;
    }

    public function rates(){

        return $this->hasMany(Rate::class);
    }
//
    public function rate()
    {
        return $this->rates->avg('rate')??0;
    }

    public function items(){

        return $this->hasMany(CartItem::class);
    }
    public function productSumNumber(){

        $item=CartItem::where('product_id',$this->id)->count();
        return $item;
    }
}
