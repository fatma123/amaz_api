<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'ar_name', 'en_name','image'
    ];
    public function sub_category(){

        return $this->hasMany(SubCategory::class,'category_id','id');
    }

    public function name()
    {
        if (app()->getLocale() == 'en')
            return $this->en_name;
        else
            return $this->ar_name;
    }

}
