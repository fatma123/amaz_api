<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaires extends Model
{
    protected $fillable =['question_id','answer','cart_id'];

    public function cart(){
        return $this->belongsTo(Cart::class)->withDefault();
    }
    public function question(){
        return $this->belongsTo(Question::class,'question_id','id');
    }
}
