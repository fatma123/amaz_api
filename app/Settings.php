<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //

    public function value()
    {
        if (app()->getLocale() == 'en')
            return $this->en_value;
        else
            return $this->ar_value;
    }
}
