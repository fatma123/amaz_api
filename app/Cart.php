<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = ['client_name', 'client_phone', 'notes', 'discount', 'status'];

    public function items()
    {
        return $this->hasMany(CartItem::class,'cart_id');
    }
}
